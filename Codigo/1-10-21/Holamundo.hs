{- main = la parte que se ejecuta de un programa
 - :: = Especificar el tipo.
 - Int = Enteros Z
 - Integer = Enteros grandotes.
 - String = Cadenas (van entre comillas)
 - Bool = True, False
 - Double = Números con punto decimal.
-}

{-
 JAVA:
   System - clase con cosas del sistema.
   System.out.println("texto") imprimir
   System.exit(-1)             Salir
 -}
 
suma :: Int -> Int -> Int -- Z x Z -> Z
suma a b = a+b

suma2 :: Double -> Double -> Double
suma2 a b = a+b

main :: IO () -- La firma es la misma
main = do
  let c = 3.5 + 4.2
  print (show c)
  putStrLn "Hola Mundofdsfdskfdskfdk"
