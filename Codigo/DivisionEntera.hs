-- divisionEntera a b = a/b
-- 10 /2 = 10 -2 -2 -... = 0
-- Wild card = comodín 
-- 0 / 1000 = 1000*0 = 0
-- 13 / 5
-- 1 + 8 / 5
-- 1 + 1 + 3 / 5
-- 1 + 1 + 0
-- 2
divisionEntera :: Int -> Int -> Int
divisionEntera _ 0 = error "No puedes dividir entre 0"
divisionEntera n m = if n < m
                     then 0
                     else 1 + (divisionEntera (n-m) m)


data Dia = Lunes | Martes | Miercoles | Jueves | Viernes | Sabado | Domingo
  deriving (Eq, Show)


esFinDeSemana :: Dia -> Bool
esFinDeSemana d = d == Viernes || d == Sabado || d == Domingo
                  
-- ||||| pipe
manana :: Dia -> Dia
-- manana d                         Guardias 
--   |d == Lunes = Martes
--   |d == Martes = Miercoles
--   |d == Miercoles = Jueves
--   |d == Jueves = Viernes
--   |d == Viernes = Sabado
--   |d == Sabado = Domingo
--   |d == Domingo = Lunes
manana d =
  case d of
    Lunes -> Martes
    Martes -> Miercoles
    Miercoles -> Jueves
    Jueves -> Viernes
    Viernes -> Sabado
    Sabado -> Domingo
    Domingo -> Lunes

 
diasParaElFin :: Dia -> Int
diasParaElFin d = if esFinDeSemana d
                  then 0
                  else 1 + (diasParaElFin (manana d))


-- [1, 10, -2, 3, -1]
-- minimo [1, 10, -2, 3, -1] = -2
-- (-2):[1, 10, 3, -1]

-- fib(100)

-- fibAux n regresa la lista con [F0, F1, ..., Fn]
fibAux :: Int -> [Int]

fibonacci n = let l = fibAux(n)
              in l !! n

