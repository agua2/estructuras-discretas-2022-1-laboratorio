par :: Int -> Bool
par n = mod n 2 == 0


--- a es divisible entre b si a % b = 0
primo :: Int -> Bool
primo 0 = False
primo 1 = False
primo n = let numeros = [2..(n-1)]
              divisores = filter (\x -> mod n x == 0) numeros
          in divisores == []


main :: IO ()
main = do {
  putStrLn "hola";
          }
