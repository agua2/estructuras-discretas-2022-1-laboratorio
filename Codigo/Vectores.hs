-- Tupla (n-ada):
-- tuples:
-- Es una estructura de datos.
-- Diferencias:
-- 1. Tamaño definido.
-- 2. Heterogéneas. Distintos tipos

type Entero = Int
type Matriz = (Int, Int, Int, Int)

{- (a, b, c, d) = (a, b)
                  (c, d)

   (a, b)    (e, f)  = (a+e, b+f)
   (c, d) +  (g, h)    (c+g, d+h)
-}

-- Suma
suma :: Matriz -> Matriz -> Matriz
suma (a1, a2, a3, a4) (b1, b2, b3, b4) = (a1 + b1, a2 + b2, a3 + b3, a4 + b4)


-- Multiplicación
multiplicacion :: Matriz -> Matriz -> Matriz
multiplicacion (a1, a2, a3, a4) (b1, b2, b3, b4) = (a1*b1+a2*b3, a1*b2+a2*b4, a3*b1+a4*b3, a3*b2+a4*b4) 

-- TAREA
-- Determinante
determinante :: Matriz -> Int
determinante = undefined

-- ¿Es invertible?
esInvertible :: Matriz -> Bool
esInvertible = undefined

-- Matriz inversa
inversa :: Matriz -> Matriz
inversa = undefined

-- [..,-4, -2, 0, 2, 4, ..]
-- Teorema
-- n es par sii n+1 es impar.
-- par 5 = not (par 4) = not (not (par 3)) = not (not (not (par 2))) = not (not (not (not (par 1)))) =
-- not (not (not (not (not (par 0))))) = not (not (not (not (not (True)))))

-- par -1 = not (par -2) = not (not (par -3)) = ...

-- Segunda auxiliar
parAux :: Int -> Bool
parAux 0 = True
parAux n = not (parAux (n-1))


-- Primera auxiliar
-- Teorema
-- n es par sii -n es par
-- par -1 = parAux 1
par :: Int -> Bool
par n = if n < 0
        then parAux ((-1)*n)
        else parAux n


{- if inútil:
   if b then True else False
   equivalente a b -}
-- Suma los elementos pares de la lista
-- [2, 4, 10, 3, 5, 7, 5, 10]
-- 2+4+10+10 = 26
-- [2, 3, 5, 7, 17, 21, 97] = 2:[3, 5, 7, 17, 21, 97]
sumaPares :: [Int] -> Int
sumaPares [] = 0
sumaPares (a:resto) = if (par a)
                      then a + (sumaPares resto)
                      else (sumaPares resto)
