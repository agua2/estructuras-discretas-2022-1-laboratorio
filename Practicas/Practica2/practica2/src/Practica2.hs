-- | Descripción: Práctica de introducción a Haskell.
module Practica2 where

{- | Ejercicio 1.
     'suma' recibe una lista de enteros y regresa la suma de sus elementos.
     Si la lista es vacía, la función regresa 0.
-}

suma :: [Int] -> Int
suma = undefined

{- | Ejercicio 2.
     'fibonacci' recibe un número n y regresa el n-ésimo número de Fibonacci
     donde los números de Fibonacci se definen como:
     F0 = 0
     F1 = 1
     Fn = Fn + F(n-1)
-}
fibonacci :: Int -> Integer
fibonacci = undefined


{- | Ejercicio 3.
     'ackermann' define la función de Ackermann.
     A(0, n) = n+1
     A(m+1, 0) = A(m, 1)
     A(m+1, n+1) = A(m, A(m+1, n))
-}
ackermann :: Int -> Integer -> Integer
ackermann = undefined

{- | Ejercicio 4.
     'parImpar' toma una lista de la forma [n1, n2, ..., nm] y regresa una lista
     de la forma [2*n1, n2, 2*n3, ..., nm] si m es par y [2*n1, n2, 2*n3, ...,
     2*nm] si m es impar. 
-}
parImpar :: [Int] -> [Int]
parImpar = undefined


{- | Ejercicio 5.
     'ordena' ordena una lista de números.
-}
ordena :: [Int] -> [Int]
ordena = undefined
