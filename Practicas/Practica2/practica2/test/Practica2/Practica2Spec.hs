{- | Descripción: Revisa que las funciones de la práctica 2 funcionen correctamente.
     Encargado: agua@ciencias.unam.mx
-}
module Practica2.Practica2Spec where

import Data.Time.Clock
import Data.Sort
import Practica2
import Test.Hspec
import Test.Tasty
import Test.Tasty.Hspec
import System.Random

-- | 'timeout' define un tiempo máximo de ejecución de 5 segundos.
timeout = mkTimeout 5000000

{- | 'randomList' recibe una semilla y regresa una lista aleatoria de números
     generada con esa semilla.
-}
randomList :: Int -> [Int]
randomList seed = 
  let gen = mkStdGen seed
      l = take 20 $ randomRs (0, 100) gen
  in l


-- | 'fib100' es la lista de los primeros 100 números de Fibonacci.
fib100 = [0,1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181,6765,10946,17711,28657,46368,75025,121393,196418,317811,514229,832040,1346269,2178309,3524578,5702887,9227465,14930352,24157817,39088169,63245986,102334155,165580141,267914296,433494437,701408733,1134903170,1836311903,2971215073,4807526976,7778742049,12586269025,20365011074,32951280099,53316291173,86267571272,139583862445,225851433717,365435296162,591286729879,956722026041,1548008755920,2504730781961,4052739537881,6557470319842,10610209857723,17167680177565,27777890035288,44945570212853,72723460248141,117669030460994,190392490709135,308061521170129,498454011879264,806515533049393,1304969544928657,2111485077978050,3416454622906707,5527939700884757,8944394323791464,14472334024676221,23416728348467685,37889062373143906,61305790721611591,99194853094755497,160500643816367088,259695496911122585,420196140727489673,679891637638612258,1100087778366101931,1779979416004714189,2880067194370816120,4660046610375530309,7540113804746346429,12200160415121876738,19740274219868223167,31940434634990099905,51680708854858323072,83621143489848422977,135301852344706746049,218922995834555169026,354224848179261915075]


-- | 'spec' es la función principal de pruebas unitarias.
spec :: Spec
spec = do
  -- Pruebas unitarias de suma.
  describe "suma" $ do
    it "regresa 0 para la lista vacía" $ do
      suma [] `shouldBe` 0
        
    it "regresa n*(n+1)/2 para la suma de 1..n" $ do
      time <- getCurrentTime >>= return . utctDayTime
      let seed = floor $ toRational $ time
      let gen = mkStdGen seed
      let (n, _) = randomR (0, 100:: Int) gen
      suma [0..n] `shouldBe` (n*(n+1)) `div` 2

    it "puede sumar una lista de elementos aleatorios" $ do
      time <- getCurrentTime >>= return . utctDayTime
      let seed = floor $ toRational $ time
      let l = (randomList seed)
      suma l `shouldBe` sum l

  describe "fibonacci" $ do
    it "regresa 0 para 0" $ do
      fibonacci 0 `shouldBe` 0
        
    it "regresa 1 para 1" $ do
      fibonacci 1 `shouldBe` 1

    it "funciona para números pequeños" $ do
      time <- getCurrentTime >>= return . utctDayTime
      let seed = floor $ toRational $ time
      let gen = mkStdGen seed
      let (n, _) = randomR (2, 20:: Int) gen
      fibonacci n `shouldBe` fib100 !! n

    it "funciona para números grandes" $ do
      time <- getCurrentTime >>= return . utctDayTime
      let seed = floor $ toRational $ time
      let gen = mkStdGen seed
      let (n, _) = randomR (80, 100:: Int) gen
      fibonacci n `shouldBe` fib100 !! n

  describe "ackermann" $ do
    it "regresa 1 para 0, 0" $ do
      ackermann 0 0 `shouldBe` 1
        
    it "regresa 2 para 1, 0" $ do
      ackermann 1 0 `shouldBe` 2

    it "regresa 3 para 2, 0" $ do
      ackermann 2 0 `shouldBe` 3

    it "regresa 2 para 0, 1" $ do
      ackermann 0 1 `shouldBe` 2

    it "regresa 3 para 0, 2" $ do
      ackermann 0 2 `shouldBe` 3

    it "regresa 3 para 1, 1" $ do
      ackermann 1 1 `shouldBe` 3

    it "regresa 7 para 2, 2" $ do
      ackermann 2 2 `shouldBe` 7

    it "regresa 9 para 2, 3" $ do
      ackermann 2 3 `shouldBe` 9

    it "regresa 61 para 3, 3" $ do
      ackermann 3 3 `shouldBe` 61

  describe "parImpar" $ do
    it "regresa [] para []" $ do
      parImpar [] `shouldBe` []

    it "funciona para listas de 1 elemento" $ do
      parImpar [5] `shouldBe` [10]

    it "funciona para otras listas" $ do
      parImpar [10, 20, 3, 5, -1, 2, 30, 0] `shouldBe` [20, 20, 6, 5, -2, 2, 60, 0]

  describe "ordena" $ do
    it "funciona para []" $ do
      ordena [] `shouldBe` []

    it "funciona para listas de 1 elemento" $ do
      time <- getCurrentTime >>= return . utctDayTime
      let seed = floor $ toRational $ time
      let gen = mkStdGen seed
      let (n, _) = randomR (0, 100:: Int) gen
      ordena [n] `shouldBe` [n]

    it "funciona para otras listas" $ do
      time <- getCurrentTime >>= return . utctDayTime
      let seed = floor $ toRational $ time
      let l = (randomList seed)
      ordena l `shouldBe` sort l



-- | Función que agrega un 'Timeout' a las pruebas que tenemos.
test_TreeTests :: IO TestTree
test_TreeTests = do 
  tests <- testSpec "pruebas" spec
  return (localOption timeout $ tests)      
