module Practica3 where

-- PARTE 1
-- | 'aplica' aplica la función dada a cada elemento de la lista.
aplica :: (a->b) -> [a] -> [b]
aplica = undefined

-- | 'filtra' filtra los elementos de la lista que no cumplen con la propiedad dada.
filtra :: (a->Bool) -> [a] -> [a]
filtra = undefined


-- | 'subsuc' obtiene todas las subsucesiones de una lista.
subsuc :: [a] -> [[a]]
subsuc = undefined

------------------------------------------------------------------------
-- PARTE 2.
-- | 'BinOp' es un tipo de dato que representa operadores binarios.
data BinOp = And | Or | Implies | Equiv
  deriving Eq

{- | 'Formula' representa una fórmula en lógica proposicional. Una fórmula es
      una variable, la negación de una fórmula o la operación de dos fórmulas.
-}
data Formula = Varp Char | Neg Formula | Binario BinOp Formula Formula 
  deriving Eq

-- | Este código sirve para que las fórmulas se puedan implimir.
instance Show Formula where
  show f = showProp f

{- | 'Interp' es un tipo de dato para representar interpretaciones. El valor de
     la variable es '1' si está en la lista y '0' en otro caso.
-}
type Interp = [Char]


-- | 'showProp' sirve para mostrar una fórmula como cadena.
showProp :: Formula -> String
showProp (Varp c) = [c]
showProp (Neg f) = "¬(" ++ (showProp f) ++ ")"
showProp (Binario b f1 f2) = let simbolo = 
                                   case b of 
                                     And -> "^"
                                     Or -> "v"
                                     Implies -> "->"
                                     Equiv -> "<->"
                             in "(" ++ (showProp f1) ++ ") " ++ simbolo ++ " (" ++ (showProp f2) ++ ")"


-- | 'vars' devuelve el conjunto de variables de una fórmula.
vars :: Formula -> [Char]
vars = undefined


{- | 'interpreta' toma una fórmula y una interpretación y devuelve el valor de
     la fórmula bajo la interpretación.
-}
interpreta :: Formula -> Interp -> Bool
interpreta = undefined


-- | 'esTautologia' nos dice si una fórmula es tautología.
esTautologia :: Formula -> Bool
esTautologia = undefined

                    
-- | 'esContradiccion' nos dice si una fórmula es contradicción.
esContradiccion :: Formula -> Bool
esContradiccion = undefined


-- | 'esContingencia' nos dice si una fórmula es contingencia.
esContingencia :: Formula -> Bool
esContingencia = undefined
