{- | Descripción: Revisa que las funciones de la práctica 3 funcionen correctamente.
     Encargado: agua@ciencias.unam.mx
-}
module Practica3.Practica3Spec where

import Data.Char
import Data.List
import Data.Time.Clock
import Practica3
import Test.Hspec
import Test.Tasty
import Test.Tasty.Hspec
import System.Random

-- | 'timeout' define un tiempo máximo de ejecución de 5 segundos.
timeout = mkTimeout 1000000


formulan :: Int -> Char -> Formula
formulan 1 c = (Varp c)
formulan n c = (Binario And (Varp c) (formulan (n-1) (chr ((ord c)+1))))


{- | 'randomList' recibe una semilla y regresa una lista aleatoria de números
     generada con esa semilla.
-}
randomList :: Int -> Int -> [Int]
randomList seed n = 
  let gen = mkStdGen seed
      l = take n $ randomRs (0, 100) gen
  in l


-- | 'spec' es la función principal de pruebas unitarias.
spec :: Spec
spec = do

  describe "aplica" $ do
    it "regresa vacía para la lista vacía" $ do
      aplica id ([]::[Int]) `shouldBe` []
    it "funciona para la función +1" $ do
      aplica (+ 1) [1..100] `shouldBe` [2..101]
    it "funciona mutliplicando por un número aleatorio" $ do
      time <- getCurrentTime >>= return . utctDayTime
      let seed = floor $ toRational $ time
      let gen = mkStdGen seed
      let (n, _) = randomR (1, 100:: Int) gen
      aplica (* n) [1..100] `shouldBe` [n, 2*n..100*n]

  describe "filtra" $ do
    it "regresa vacía para la lista vacía" $ do
      filtra (\_->True) ([]::[Int]) `shouldBe` []
    it "funciona para quitar pares" $ do
      filtra (\x-> mod x 2 == 1) [1..100] `shouldBe` [1, 3..99]
    it "si todos los elementos cumplen, devuelve la lista total" $ do
      filtra (\_->True) [1..100] `shouldBe` [1..100]
    it "si ningún elemento cumple, devuelve la lista vacía" $ do
      filtra (\_->False) [1..100] `shouldBe` []

  describe "vars" $ do
    it "regresa una lista unitaria para una variable" $ do
      vars (Varp 'a') `shouldBe` ['a']
    it "no repite variables" $ do
      vars (Binario Implies (Varp 'a') (Binario Implies (Varp 'a') (Varp 'a'))) `shouldBe` ['a']
    it "funciona para fórmulas grandes" $ do
      vars (formulan 26 'a') `shouldMatchList` ['a'..'z']   

  describe "subsuc" $ do
    it "funciona para la lista vacía" $ do
      subsuc ([]::[Int]) `shouldBe` [[]]
    it "funciona para listas pequeñas" $ do
      time <- getCurrentTime >>= return . utctDayTime
      let seed = floor $ toRational $ time
      let lista = randomList seed 5
      subsuc lista `shouldMatchList` subsequences lista
    it "funciona para listas 'grandes'" $ do
      time <- getCurrentTime >>= return . utctDayTime
      let seed = floor $ toRational $ time
      let lista = randomList seed 11
      subsuc lista `shouldMatchList` subsequences lista

  describe "interpreta" $ do
    it "funciona para variables" $ do
      let f = (Varp 'p')
      interpreta f ['p'] `shouldBe` True
      interpreta f ['q'] `shouldBe` False
    it "funciona para conjunciones" $ do
      let f = (Binario And (Varp 'p') (Varp 'q'))
      interpreta f ['p', 'q'] `shouldBe` True
      interpreta f ['q'] `shouldBe` False
      interpreta f [] `shouldBe` False
      interpreta f ['p', 'r'] `shouldBe` False
    it "funciona para disyunciones" $ do
      let f = (Binario Or (Varp 'p') (Varp 'q'))
      interpreta f ['p', 'q'] `shouldBe` True
      interpreta f ['q'] `shouldBe` True
      interpreta f [] `shouldBe` False
      interpreta f ['p', 'r'] `shouldBe` True
    it "funciona para implicaciones" $ do
      let f = (Binario Implies (Varp 'p') (Varp 'q'))
      interpreta f ['p', 'q'] `shouldBe` True
      interpreta f ['q'] `shouldBe` True
      interpreta f [] `shouldBe` True
      interpreta f ['p', 'r'] `shouldBe` False
    it "funciona para equivalencias" $ do
      let f = (Binario Equiv (Varp 'p') (Varp 'q'))
      interpreta f ['p', 'q'] `shouldBe` True
      interpreta f ['q'] `shouldBe` False
      interpreta f [] `shouldBe` True
      interpreta f ['p', 'r'] `shouldBe` False
    it "funciona en general" $ do
      let f = (Binario And (Neg (Varp 'a')) (Binario Equiv (Binario Or (Varp 'p') (Varp 'q')) (Binario Implies (Varp 's') (Varp 't'))))
      interpreta f [] `shouldBe` False
      interpreta f ['p', 'q', 'w'] `shouldBe` True

  describe "tautología, contingencia, contradicción" $ do
    it "funciona para q->p->q" $ do
      let f = (Binario Implies (Varp 'q') (Binario Implies (Varp 'p') (Varp 'q')))
      esTautologia f `shouldBe` True
      esContingencia f `shouldBe` False
      esContradiccion f `shouldBe` False
    it "funciona para ((p->¬q) ^ (s -> q) ^ s) -> p" $ do
      let f = (Binario Implies (Binario And (Binario And (Binario Implies (Varp 'p') (Neg (Varp 'q'))) (Binario Implies (Varp 's') (Varp 'q'))) (Varp 's')) (Varp 'p'))
      esTautologia f `shouldBe` False
      esContingencia f `shouldBe` True
      esContradiccion f `shouldBe` False
    it "funciona para p^¬p" $ do
      let f = (Binario And (Varp 'p') (Neg (Varp 'p')))
      esTautologia f `shouldBe` False
      esContingencia f `shouldBe` False
      esContradiccion f `shouldBe` True
   

-- | Función que agrega un 'Timeout' a las pruebas que tenemos.
test_TreeTests :: IO TestTree
test_TreeTests = do 
  tests <- testSpec "pruebas" spec
  return (localOption timeout $ tests)      
