module Practica4 where

-- PARTE 2

-- | Tipo de dato que representa a una matriz de 2x2.
type Matriz = (Double, Double, Double, Double)

-- | 'determinante' regresa el determinante de una matriz.
determinante :: Matriz -> Double
determinante = undefined


-- | 'esInvertible' nos dice si la matriz es invertible.
esInvertible :: Matriz -> Bool
esInvertible = undefined


-- | 'inversa' calcula la matriz inversa.
inversa :: Matriz -> Matriz
inversa = undefined



-- PARTE 3

-- | 'reps' cuenta el número de repeticiones de un elemento en una lista.
reps :: Eq a => [a] -> [(a, Int)]
reps = undefined


-- | 'quitaReps' quita elementos repetidos en una lista.
quitaReps :: Eq a => [a] -> [a]
quitaReps = undefined


-- | 'productoCartesiano' regresa el producto cartesiano de dos lisas.
productoCartesiano :: [a] -> [b] -> [(a, b)]
productoCartesiano = undefined


-- | 'esSublista' nos dice si una lista es sublista de otra.
esSublista :: Eq a => [a] -> [a] -> Bool
esSublista = undefined



