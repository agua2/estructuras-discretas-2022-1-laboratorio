{- | Descripción: Revisa que las funciones de la práctica 4 funcionen correctamente.
     Encargado: agua@ciencias.unam.mx
-}
module Practica4.Practica4Spec where

import Control.Exception (evaluate)
import Data.Time.Clock
import Practica4
import Test.Hspec
import Test.Tasty
import Test.Tasty.Hspec
import System.Random

-- | 'timeout' define un tiempo máximo de ejecución de 5 segundos.
timeout = mkTimeout 1000000

-- | Pruebas de matrices.
specMatrices :: Spec
specMatrices = do
  describe "determinante" $ do
    it "regresa 0 para matrices no invertibles" $ do
      determinante (0, 0, 0, 0) `shouldBe` 0
      determinante (1, 2, 2, 4) `shouldBe` 0
    it "regresa algo distinto de 0 para matrices invertibles" $ do
      determinante (2, 4, 6, 8) `shouldNotBe` 0
      determinante (1, 0, 0, 1) `shouldNotBe` 0

  describe "esInvertible" $ do
    it "regresa False para matrices no invertibles" $ do
      esInvertible (0, 0, 0, 0) `shouldBe` False
      esInvertible (1, 2, 2, 4) `shouldBe` False
    it "regresa True para matrices invertibles" $ do
      esInvertible (2, 4, 6, 8) `shouldBe` True
      esInvertible (1, 0, 0, 1) `shouldBe` True

  describe "inversa" $ do
    it "regresa error para matrices no invertibles" $ do
      evaluate (inversa (0, 0, 0, 0)) `shouldThrow` anyErrorCall
      evaluate (inversa (1, 2, 2, 4)) `shouldThrow` anyErrorCall
    it "invierte correctamente" $ do
      inversa (2, 4, 6, 8) `shouldBe` (-1, 1/2, 3/4, -1/4)
      inversa (1, 0, 0, 1) `shouldBe` (1, 0, 0, 1)

-- | Parte 3: Funciones sobre listas.
specListas :: Spec
specListas = do
  describe "reps" $ do
    it "funciona para la lista vacía" $ do
      reps ([]::[Int]) `shouldBe` []
    it "funciona para otras listas" $ do
      reps [11, 1, 3, 4, 5, 9, 9, 2, 3, 1, 3, 3, 5] `shouldMatchList` [(11,1),(1,2),(3,4),(4,1),(5,2),(9,2),(2,1)]

  describe "quitaReps" $ do
    it "no hace nada con la vac\'ia" $ do
      quitaReps ([]::[Int]) `shouldBe` []
    it "quita las repeticiones en los ejemplos del pdf" $ do
      quitaReps [11, 1, 3, 4, 5, 9, 9, 2, 3, 1, 3, 3, 5] `shouldMatchList` [11,4,9,2,1,3,5]
      quitaReps [1, 1, 1, 3, 4, 5, 1, 3, 9] `shouldMatchList` [4,5,1,3,9]
    it "funciona para listas con un solo elemento repetido" $ do
      time <- getCurrentTime >>= return . utctDayTime
      let seed = floor $ toRational $ time
      let gen = mkStdGen seed
      let (n, gen2) = randomR (1, 100:: Int) gen
      let (m, _) = randomR (1, 100:: Int) gen2
      quitaReps (replicate n m) `shouldBe` [m] 
    it "funciona para listas cicladas" $ do
      time <- getCurrentTime >>= return . utctDayTime
      let seed = floor $ toRational $ time
      let gen = mkStdGen seed
      let (n, gen2) = randomR (1, 100:: Int) gen
      let (m, _) = randomR (1, 100:: Int) gen2
      quitaReps (take n (cycle [1..m])) `shouldMatchList` [1..(min n m)]

  describe "productoCartesiano" $ do
    it "funciona para vacía" $ do
      productoCartesiano ([]::[Int]) [1..100] `shouldBe` []
      productoCartesiano [1..100] ([]::[Int]) `shouldBe` []
    it "funciona para otros casos" $ do
      productoCartesiano [1, 2, 3, 4] [4, 4, 5, 3] `shouldMatchList` [(1,4),(1,4),(1,5),(1,3),(2,4),(2,4),(2,5),(2,3),(3,4),(3,4),(3,5),(3,3),(4,4),(4,4),(4,5),(4,3)]
      productoCartesiano [2, 3, 4, 5] [1, 2, 3, 4, 5] `shouldMatchList` [(2,1),(2,2),(2,3),(2,4),(2,5),(3,1),(3,2),(3,3),(3,4),(3,5),(4,1),(4,2),(4,3),(4,4),(4,5),(5,1),(5,2),(5,3),(5,4),(5,5)]
  
  describe "esSublista" $ do
    it "funciona para vacía" $ do
      esSublista [] [1..100] `shouldBe` True
      esSublista [] ([]::[Int]) `shouldBe` True
    it "funciona en otros casos" $ do
      esSublista [1, 1, 2, 3] [1,2,3, 4, 1, 2, 2, 3, 5] `shouldBe` True
      esSublista [1, 1, 2, 3] [1,2,3, 4, 0, 2, 2, 3, 5] `shouldBe` False
      esSublista [1, 1, 2, 3] [1,2,3, 4, 1] `shouldBe` False
      
-- | Función que agrega un 'Timeout' a las pruebas que tenemos.
test_TreeTests :: IO TestTree
test_TreeTests = do 
  testsM <- testSpec "pruebas de matrices" specMatrices
  testsL <- testSpec "pruebas de listas" specListas
  return (localOption timeout $ testGroup
           "pruebas" [testsM, testsL]) 
